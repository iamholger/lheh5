#!/bin/bash

FIN=$1 #../../pythia8-diy/test/Wm8-NONE.h5
FOUT=$2 # myoutputprefix

parallel --bar  h5repack  -l CHUNK={1} -f GZIP={2} $FIN ${FOUT}_{1}_{2}.h5  ::: {300,1000,3000,10000,30000,100000} ::: {0..9}
