#include "lheh5.h"

using namespace lheh5;

int main(int argc, char **argv) {
   
   // Input sanity check
   if (argc<3) {
     std::cout << "ERROR: Not enough arguments provided\n\n";
     std::cout << "Usage:\n\t " << argv[0] << "  INPUT.h5  EVENTS_PER_READ  [SLEEPTIME_IN_MILLISECONDS]" << "\n\n";
     exit(1);
   }

   // The usual MPI stuff
   int rank, size;
   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   double starttime, endtime;

   // The batch-size i.e. number of events to read per read operation
   size_t const batchsize = atoi(argv[2]);

   if (rank==0) {
     std::cout << "INFO: Reading events from file " << argv[1] <<"\n";
     std::cout << "INFO: Reading " << batchsize << " events at a time\n";
   }
   
   // Open file for reading on all ranks 
   File file(argv[1], File::ReadOnly, MPIOFileDriver(MPI_COMM_WORLD, MPI_INFO_NULL));
   
   // This is for the scaling test where we emulate event processing by sleep
   unsigned int sleep_base = 0;
   if (argc>=5) {
     if (rank==0) std::cout << "Sleep time set to " << sleep_base << " ms per event\n";
     sleep_base=atoi(argv[4]);
   }

   // Connect with groups
   Group _index    = file.getGroup("index");
   Group _particle = file.getGroup("particle");
   Group _event    = file.getGroup("event");

   // Connect with dataspace to get global number of events
   long int nEvents;
   hid_t dspace = H5Dget_space(file.getDataSet("index/start").getId());
   if (rank ==0) {
      nEvents =  H5Sget_simple_extent_npoints(dspace);
      std::cout << "Total number of ranks: " << size << "\n";
      std::cout << "File contains " << nEvents << " events\n";
   }
   // Read only the first n events
   unsigned int nread = 0;
   if (argc>=4) {
     nread=atoi(argv[3]);
   }
   if (nread < nEvents) nEvents=nread;


   // Broadcast nEvents to all other ranks
   MPI_Bcast(&nEvents,   1, MPI_LONG_INT, 0, MPI_COMM_WORLD);
   MPI_Barrier(MPI_COMM_WORLD);
  
   // Partitioning arithmetic starts here 
   size_t const iStart = rank * nEvents/size;
   size_t iStop;
   if (rank==size-1)  iStop=nEvents-1;
   else iStop = (rank+1) * nEvents/size -1;
   size_t const nEvents_rank = iStop-iStart + 1;
  
   bool debug=true;
   if (nEvents_rank>10) debug=false;

   for(int i = 0; i < size; i++) {
      MPI_Barrier(MPI_COMM_WORLD);
      if (i == rank) {
         std::cout << "[" << rank <<"]  reads maximally " << nEvents_rank << " events --- start/stop: " << iStart << "/" << iStop<<"\n";
      }
   } 

   // Balance reading of all events amongs ranks
   std::vector<std::vector<size_t> > partition;
   if (batchsize >= nEvents_rank) partition = {{iStart,nEvents_rank}};
   else {
      if (nEvents_rank%batchsize == 0) {
         for (unsigned int f=0;f< nEvents_rank/batchsize;++f) partition.push_back({iStart + f*batchsize, batchsize});
      }
      else {
         for (unsigned int f=0;f< nEvents_rank/batchsize;++f) partition.push_back({iStart+f*batchsize, batchsize});
         partition.push_back({partition.back()[0]+batchsize, nEvents_rank%batchsize});
      }
   }
   
   int ranksum = 0; 
   if (debug) {
      for (int r=0; r<size;++r) {
            MPI_Barrier(MPI_COMM_WORLD);
         if (rank == r) {
            for (auto p : partition) {
               ranksum+=p[1];
            }
         }
      }
   }
   
   // Cross-check --- count events and particles read and compare with size of dataset at the end
   int test(0);
   int testp(0);
   MPI_Barrier(MPI_COMM_WORLD);
   Events e;
   for (auto p : partition) {
      //MPI_Barrier(MPI_COMM_WORLD);
      starttime = MPI_Wtime();
      e = readEvents(_index, _particle, _event, p[0], p[1]);
      endtime   = MPI_Wtime();
      printf("[%i] read took %f seconds\n",rank, endtime-starttime);
   
      // Event-loop here
      unsigned int _nevents = e._vnparticles.size(); // NOTE: could have used any other event property
      for (unsigned int i=0;i<_nevents;++i) {
         if (debug){
           EventHeader hd = e.mkEventHeader(i);
             std::cout << "Event # " << i << " on rank [" <<rank << "]:\n";
             std::cout << hd;
           //Particle loop 
           for (auto part: e.mkEvent(i)) {
              std::cout << part;
              //double _px = part.px; // This is how to access individual particle quantities
           }
         }
      }
   
      // Increment this rank's counters
      test+=e._vstart.size();
      testp+=e._vid.size();
   
      // Sleep for nevents * sleep_base ms
      unsigned int sleeptime = p[1]*1000*sleep_base;
      usleep(sleeptime); // sleeptime is in microseconds
   
   
   }
   
   MPI_Barrier(MPI_COMM_WORLD);
   
   // Collective operation to get the global sum of all rank counters
   int global_sum;
   int global_sump;
   MPI_Reduce(&test,  &global_sum,  1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&testp, &global_sump, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Barrier(MPI_COMM_WORLD);
   
   if (rank == 0) {
      std::cout << "Global sum of events    read: " << global_sum << " (should be " <<nEvents <<")\n";
      std::cout << "Global sum of particles read: " << global_sump << "\n";
   }
        

    MPI_Finalize();
    return 0; // successfully terminated
}

