#include "lheh5.h"

using namespace lheh5;

int main(int argc, char **argv) {
   
   // Input sanity check
   if (argc<3) {
     std::cout << "ERROR: Not enough arguments provided\n\n";
     std::cout << "Usage:\n\t " << argv[0] << "  INPUT.h5  EVENTS_PER_READ  [SLEEPTIME_IN_MILLISECONDS]" << "\n\n";
     exit(1);
   }

   int rank(0), size(1);

   double starttime, endtime;

   // The batch-size i.e. number of events to read per read operation
   size_t const batchsize = atoi(argv[2]);

   if (rank==0) {
     std::cout << "INFO: Reading events from file " << argv[1] <<"\n";
     std::cout << "INFO: Reading " << batchsize << " events at a time\n";
   }
   
   // Open file for reading
   File file(argv[1], File::ReadOnly);;
   
   // This is for the scaling test where we emulate event processing by sleep
   unsigned int sleep_base = 0;
   if (argc>=5) {
     if (rank==0) std::cout << "Sleep time set to " << sleep_base << " ms per event\n";
     sleep_base=atoi(argv[4]);
   }

   // Connect with groups
   //Group _index    = file.getGroup("index");
   Group _particle = file.getGroup("particle");
   Group _event    = file.getGroup("event");

   // Connect with dataspace to get global number of events
   long int nEvents;
   int npLO, npNLO;
   hid_t dspace = H5Dget_space(file.getDataSet("event/start").getId());
   // Read only the first n events
   unsigned int nread = 0;
   if (rank ==0) {
      nEvents =  H5Sget_simple_extent_npoints(dspace);
      if (argc>=4) {
        nread=atoi(argv[3]);
      }
      if (nread < nEvents) nEvents=nread;
      std::cout << "Total number of ranks: " << size << "\n";
      std::cout << "File contains " << nEvents << " events\n";
      Group _procInfo = file.getGroup("procInfo");
      DataSet _npLO   =  _procInfo.getDataSet("npLO");
      DataSet _npNLO  =  _procInfo.getDataSet("npNLO");
      _npLO.read(npLO);
      _npNLO.read(npNLO);

   }

  
   // Partitioning arithmetic starts here 
   size_t const iStart(0), iStop(nEvents-1);

   // Balance reading of all events amongs ranks
   std::vector<std::vector<size_t> > partition;
   if (batchsize >= nEvents) partition = {{iStart,nEvents}};
   else {
      if (nEvents%batchsize == 0) {
         for (unsigned int f=0;f< nEvents/batchsize;++f) partition.push_back({iStart + f*batchsize, batchsize});
      }
      else {
         for (unsigned int f=0;f< nEvents/batchsize;++f) partition.push_back({iStart+f*batchsize, batchsize});
         partition.push_back({partition.back()[0]+batchsize, nEvents%batchsize});
      }
   }
   
   // Cross-check --- count events and particles read and compare with size of dataset at the end
   int test(0), testp(0);
   Events2 e;
   for (auto p : partition) {
      //MPI_Barrier(MPI_COMM_WORLD);
      e = readEvents(_particle, _event, p[0], p[1], npLO, npNLO);
      // Event-loop here ...
   
      // Increment counters
      test +=e._vstart.size();
      testp+=e.   _vid.size();
   }
   
   
   // Collective operation to get the global sum of all rank counters
   
   if (rank == 0) {
      std::cout << "Global sum of events    read: " << test << " (should be " <<nEvents <<")\n";
      std::cout << "Global sum of particles read: " << testp << "\n";
   }
        
    return 0;
}

