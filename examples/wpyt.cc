// main20.cc is a part of the PYTHIA event generator.
// Copyright (C) 2018 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program. It shows how PYTHIA 8 can write
// a Les Houches Event File based on its process-level events.

#include "Pythia8/Pythia.h"
#include "lheh5.h"



using namespace Pythia8;

class LHAupH5 : public Pythia8::LHAup {
  public:
    LHAupH5( HighFive::File* file_, size_t firstEvent, size_t readSize) : _numberRead(0) {
      file = file_;
      _index       = file->getGroup("index");
      _particle    = file->getGroup("particle");
      _event       = file->getGroup("event");
      _init        = file->getGroup("init");
      _procInfo    = file->getGroup("procInfo");
      // This reads and holds the information of readSize events, starting from firstEvent
      lheevents = lheh5::readEvents(_index, _particle, _event, firstEvent, readSize);
    }
  
       
    void setScalesFromLHEF(bool b) { setScalesFromLHEF_ = b; }
    
    // Read and set the info from init and procInfo
    bool setInit() override;
    bool setEvent(int idProc=0) override;
  
  private:

    HighFive::File*                         file;
    // Connect with groups
    HighFive::Group                         _index, _particle, _event, _init, _procInfo;
    lheh5::Events                        lheevents;
    int                                     _numberRead;
  

    // Flag to set particle production scales or not.
    bool setScalesFromLHEF_;

};

bool LHAupH5::setInit()
{
  /*   if (!runInfo) return false;
       const HEPRUP &heprup = *runInfo->getHEPRUP();*/
   
   int beamA, beamB;
   double energyA, energyB;
   int PDFgroupA, PDFgroupB;
   int PDFsetA, PDFsetB;

   _init.getDataSet("beamA")    .read(beamA);
   _init.getDataSet("energyA")  .read(energyA);
   _init.getDataSet("PDFgroupA").read(PDFgroupA);
   _init.getDataSet("PDFsetA")  .read(PDFsetA);
   
   _init.getDataSet("beamB")    .read(beamB);
   _init.getDataSet("energyB")  .read(energyB);
   _init.getDataSet("PDFgroupB").read(PDFgroupB);
   _init.getDataSet("PDFsetB")  .read(PDFsetB);

   setBeamA(beamA, energyA, PDFgroupA, PDFsetA);
   setBeamB(beamB, energyB, PDFgroupB, PDFsetB);
   
   int weightingStrategy;
   _init.getDataSet("weightingStrategy").read(weightingStrategy);
   setStrategy(weightingStrategy);
   
   int numProcesses;
   _init.getDataSet("numProcesses").read(numProcesses);

   vector<int> procId;        // NOTE: C++17 allows int[numProcesses]
   vector<double> xSection;   // NOTE: C++17 allows double[numProcesses]
   vector<double> error;      // NOTE: C++17 allows double[numProcesses]
   vector<double> unitWeight; // NOTE: C++17 allows double[numProcesses]
   _procInfo.getDataSet("procId").read(procId);
   _procInfo.getDataSet("xSection").read(xSection);
   _procInfo.getDataSet("error").read(error);
   _procInfo.getDataSet("unitWeight").read(unitWeight);
   for (size_t np=0; np<numProcesses;++np) {
     addProcess(procId[np], xSection[np], error[np], unitWeight[np]);
     std::cout << "Adding process: " << procId[np] << " "<< xSection[np] << " " << error[np] << " "<< unitWeight[np] <<"\n";
   }

  return true;
}

// TODO actually use idProc???
bool LHAupH5::setEvent(int idProc)
{

  lheh5::EventHeader eHeader = lheevents.mkEventHeader( _numberRead );

  setProcess(eHeader.pid,eHeader.weight,eHeader.scale,eHeader.aqed,eHeader.aqcd);

  double scalein = -1.;
  for (auto part : lheevents.mkEvent( _numberRead ) ) {
    addParticle(part.id,part.status,part.mother1,part.mother2,part.color1,part.color2,
		part.px,part.py,part.pz,part.e,part.m,part.lifetime,part.spin,scalein);
  }
  if( _numberRead < 10 ) listEvent();
  _numberRead++;


  return true;
}

int main(int argc, char **argv) {


  // Input sanity check
  if (argc<2) {
    std::cout << "ERROR: Not enough arguments provided\n\n";
    std::cout << "Usage:\n\t " << argv[0] << "  INPUT.h5  EVENTS_PER_READ  [SLEEPTIME_IN_MILLISECONDS]" << "\n\n";
    exit(1);
  }

  
  // Generator.
  Pythia pythia;

  HighFive::File file(argv[1], HighFive::File::ReadOnly);  

  if( argc > 2 ) pythia.readFile(argv[2]);
  int numEvents = pythia.settings.mode("Main:numberOfEvents");
  if( numEvents < 1 ) numEvents = 100;

  size_t eventOffset = 0;
  
  // Create an LHAup object that can access relevant information in pythia.
  LHAupH5* LHAup = new LHAupH5( &file , eventOffset, numEvents);

  // Give the external reader to Pythia
  pythia.setLHAupPtr(LHAup);

  // Tell Pythia that the we are handling the LHE information
  pythia.settings.mode("Beams:frameType", 5);

  pythia.init();
  
  // Loop over events.
  for (int i = 0; i < numEvents; ++i) {
    // Generate an event.
    pythia.next();
  }

  // Statistics: full printout.
  pythia.stat();

  delete LHAup;
  // Done.
  return 0;
}
